package main

import (
	"fmt"
	"regexp"
)

func WordCount(value string) int {
	re := regexp.MustCompile(`[\S]+`)

	results := re.FindAllString(value, -1)
	return len(results)
}

func main() {

	firstName := "arthur"

	fmt.Println(firstName)

	ptr := &firstName
	fmt.Println(ptr, *ptr)

	firstName = "Tricia"
	fmt.Println(ptr, *ptr)

	fmt.Println(WordCount("To be or not to be"))
}
